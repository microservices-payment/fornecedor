package br.com.weverton.microservice.fornecedor.fornecedor.controller;

import java.util.List;

import br.com.weverton.microservice.fornecedor.fornecedor.dto.ItemDoPedidoDTO;
import br.com.weverton.microservice.fornecedor.fornecedor.model.Pedido;
import br.com.weverton.microservice.fornecedor.fornecedor.service.PedidoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("pedido")
public class PedidoController {
    private static final Logger LOG = LoggerFactory.getLogger(PedidoController.class);
    @Autowired
    private PedidoService pedidoService;

    @RequestMapping(method = RequestMethod.POST)
    public Pedido realizaPedido(@RequestBody List<ItemDoPedidoDTO> itemDoPedidoDTOS) {
        LOG.info("Iniciando os pedidos");
        return pedidoService.realizaPedido(itemDoPedidoDTOS);
    }

    @RequestMapping("/{id}")
    public Pedido getPedidoPorId(@PathVariable Long id) {
        return pedidoService.getPedidoPorId(id);
    }
}
