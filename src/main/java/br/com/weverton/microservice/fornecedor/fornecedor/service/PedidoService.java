package br.com.weverton.microservice.fornecedor.fornecedor.service;

import java.util.List;
import java.util.stream.Collectors;

import br.com.weverton.microservice.fornecedor.fornecedor.controller.PedidoController;
import br.com.weverton.microservice.fornecedor.fornecedor.dto.ItemDoPedidoDTO;
import br.com.weverton.microservice.fornecedor.fornecedor.model.Pedido;
import br.com.weverton.microservice.fornecedor.fornecedor.model.PedidoItem;
import br.com.weverton.microservice.fornecedor.fornecedor.model.Produto;
import br.com.weverton.microservice.fornecedor.fornecedor.repository.PedidoRepository;
import br.com.weverton.microservice.fornecedor.fornecedor.repository.ProdutoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PedidoService {
    private static final Logger LOG = LoggerFactory.getLogger(PedidoController.class);


    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Pedido realizaPedido(List<ItemDoPedidoDTO> itens) {
        LOG.info("realizando o(s) pedido(s) =>");

        if (itens == null) {
            return null;
        }

        List<PedidoItem> pedidoItens = toPedidoItem(itens);
        Pedido pedido = new Pedido(pedidoItens);
        pedido.setTempoDePreparo(itens.size());
        return pedidoRepository.save(pedido);
    }

    public Pedido getPedidoPorId(Long id) {
        return this.pedidoRepository.findById(id).orElse(new Pedido());
    }

    private List<PedidoItem> toPedidoItem(List<ItemDoPedidoDTO> itens) {

        List<Long> idsProdutos = itens
                .stream()
                .map(item -> item.getId())
                .collect(Collectors.toList());

        List<Produto> produtosDoPedido = produtoRepository.findByIdIn(idsProdutos);

        List<PedidoItem> pedidoItens = itens
                .stream()
                .map(item -> {
                    Produto produto = produtosDoPedido
                            .stream()
                            .filter(p -> p.getId() == item.getId())
                            .findFirst().get();

                    PedidoItem pedidoItem = new PedidoItem();
                    pedidoItem.setProduto(produto);
                    pedidoItem.setQuantidade(item.getQuantidade());
                    return pedidoItem;
                })
                .collect(Collectors.toList());
        return pedidoItens;
    }
}
