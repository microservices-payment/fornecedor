package br.com.weverton.microservice.fornecedor.fornecedor.model;

public enum PedidoStatus {
	RECEBIDO, PRONTO, ENVIADO;
}
