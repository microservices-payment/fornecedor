package br.com.weverton.microservice.fornecedor.fornecedor.service;

import br.com.weverton.microservice.fornecedor.fornecedor.model.InfoFornecedor;
import br.com.weverton.microservice.fornecedor.fornecedor.repository.InfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class InfoService {

    private static final Logger LOG = LoggerFactory.getLogger(InfoService.class);

    @Autowired
    private InfoRepository infoRepository;

    public InfoFornecedor getInfoPorEstado(String estado) {
        LOG.info("Buscar fornecedor por estado na base =>");
        return infoRepository.findByEstado(estado);
    }
}
